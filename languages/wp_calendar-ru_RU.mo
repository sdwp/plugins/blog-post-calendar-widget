��                �      �     �           1     5     ;     B     U     ^     g     o     t     y          �     �     �     �     �     �     �     �     �  	   �               '     :     @     G  '  `  !   �  B   �     �     �               !     0     ?     L     U     ^     m     v     }  &   �     �     �     �     �  #     %   +     Q     b  ?   �  1   �     �       $      &laquo; Prev Month A calendar widget to show posts. All April August Blog Post Calendar December February January July June Large March May Medium Next Month &raquo; November October Select Calendar size: Select Post type: Select a category Select a term September Show Author: Show Comment Count: Show future posts: Small Title: Visit %s&#8217;s website Project-Id-Version: WP Calender
POT-Creation-Date: 2013-05-04 01:17+0530
PO-Revision-Date: 2021-03-12 11:21+0000
Last-Translator: 
Language-Team: Русский
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-KeywordsList: _e;__
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: .
Report-Msgid-Bugs-To: 
Language: ru_RU
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
X-Loco-Version: 2.5.2; wp-5.7 «Предыдущий месяц Виджет календаря для показа страниц Все Апрель Август Blog Post Calendar Декабрь Февраль Январь Июль Июнь Большой Март Май Средний Следующий  месяц &raquo; Ноябрь Октябрь Выберите размер: Выберите тип: Выберите категорию Выберите таксономию Сентябрь Показать автора: Показать количество комментариев: Показать будущие страницы: Маленький Заголовок: Посетите %s&#8217;s сайт 